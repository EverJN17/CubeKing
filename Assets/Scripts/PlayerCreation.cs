﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCreation : MonoBehaviour
{
    public static PlayerCreation instance;
    public GameObject cube;
    public GameObject[] enemies;
    public List<GameObject> playerCubes = new List<GameObject>();
    int totalPlayerCreations;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        totalPlayerCreations = GameManager.instance.playerCreationCubes;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && GameManager.instance.gameStart)
        {
            RaycastHit hit;
            Ray clickedPos = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(clickedPos, out hit, Mathf.Infinity))
            {
                if (hit.collider.tag.Equals("can"))
                {
                    hit.collider.tag = "done";
                    hit.collider.enabled = false;
                    GameObject temp = Instantiate(cube, new Vector3(hit.transform.position.x, hit.transform.position.y+1, hit.transform.position.z), Quaternion.identity);
                    if (!playerCubes.Contains(temp))
                        playerCubes.Add(temp);
                    if (totalPlayerCreations <= 0)
                    {
                        GameUI.instance.MovesCounter(totalPlayerCreations);
                        StartCoroutine("ActivatingPlayers");
                        enabled = false;
                    }
                    else {
                        totalPlayerCreations--;
                        GameUI.instance.MovesCounter(totalPlayerCreations + 1);
                        StartCoroutine("DeactivatingPlayers");
                    }
                    temp.tag = "Player";
                    temp.name = "MainPlayer";
                    temp.GetComponent<MeshRenderer>().material.color = new Color(0.1568f,1,0,1);
                    temp.transform.parent = GameObject.Find("Players").transform;
                    GameObject.Find("Players").GetComponent<MainParent>().mainPlayer = temp;
                }
            }
        }
    }


    IEnumerator ActivatingPlayers()
    {
        enemies = GameManager.instance.enemyCubes;
        for (int i = 0; i < playerCubes.Count; i++)
        {
            playerCubes[i].GetComponent<Multiplier>().enabled = true;
        }
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].GetComponent<Multiplier>().enabled = true;
        }
       
        yield return null;
    }

    IEnumerator DeactivatingPlayers()
    {
        enemies = GameManager.instance.enemyCubes;
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].GetComponent<Multiplier>().enabled = false;
        }
        for (int i = 0; i < playerCubes.Count; i++)
        {
            playerCubes[i].GetComponent<Multiplier>().enabled = false;
        }
        yield return null;
    }

    IEnumerator SettingGridColliderOff()
    {
        yield return new WaitForSeconds(5f);
        List<GameObject> temp = new List<GameObject>();
        temp = Grid.instance.gridObj;
        for (int i = 0; i < temp.Count; i++)
        {
            DestroyImmediate(temp[i].GetComponent<Collider>());
        }
        Grid.instance.StartCoroutine("PercentageCalculator");
    }
}
