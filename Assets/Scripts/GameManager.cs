﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public List<GameObject> mainObjects = new List<GameObject>(),allcubes;
    public GameObject confetti;
    public int totalPlayers, playerCreationCubes;
    public GameObject[] enemyCubes;
    public bool gameStart;
    
    private void Awake()
    {
        instance = this;
        totalPlayers = mainObjects.Count;
        enemyCubes = GameObject.FindGameObjectsWithTag("enemy");
        gameStart = false;
    }

    // Start is called before the first frame update
    void Start()
    {
      
        
    }

    public bool CheckForPercentageCalculator()
    {
        for (int i = 0; i < mainObjects.Count; i++)
        {
            if (!mainObjects[i].GetComponent<MainParent>().fillBubble.GetComponent<FillBubbleTop>().calCompleted)
            {
                return false;
            }
        }
        return true;
    }

    public void CheckLevelCompleteORFail()
    {
        mainObjects = mainObjects.OrderBy(item => item.GetComponent<MainParent>().thisPlayerPercentage).ToList();

        if (mainObjects[mainObjects.Count-1].GetComponent<MainParent>().mainPlayer.tag.Equals("Player"))
        {
            // levelComplete
            // print("complete");
            mainObjects[mainObjects.Count - 1].GetComponent<MainParent>().fillBubble.transform.DOScale(new Vector3(0.5f,0.5f,0.5f), 0.15f);
            mainObjects[mainObjects.Count - 1].GetComponent<MainParent>().fillBubble.GetComponent<FillBubbleTop>().crown.enabled = true;
            GameUI.instance.Invoke("LevelCompleted",1f);
        }
        else {
            GameUI.instance.LevelFailed();
            //print("levelfail");
        }
    }

    public void GettingAllCubes()
    {
        for (int i = 0; i < mainObjects.Count; i++)
        {
            foreach (Transform item in mainObjects[i].transform)
            {
                if (!allcubes.Contains(item.gameObject)) {
                    allcubes.Add(item.gameObject);
                }
            }
        }
       
    }

}
