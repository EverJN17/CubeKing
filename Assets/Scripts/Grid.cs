﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Grid : MonoBehaviour
{
    public static Grid instance;
    public List<GameObject> gridObj = new List<GameObject>();
    public GameObject emptyObj, bubblePop;
    public int enemyPercentage, playerPercentage;
    List<GameObject> tempPercentageCal = new List<GameObject>();
    bool once;

    private void Awake()
    {
        instance = this;
        foreach (Transform trans in transform)
        {
            gridObj.Add(trans.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void CheckForLevelCompleteORFail()
    {
        tempPercentageCal.Clear();
        for (int i = 0; i < gridObj.Count; i++)
        {
            if (gridObj[i].tag == "done")
            {
                tempPercentageCal.Add(gridObj[i]);
            }
        }

        if (gridObj.Count - 1 == tempPercentageCal.Count)
        {
           // print("level checked");
            StartCoroutine("PercentageCalculator");
        }
    }

    
    IEnumerator PercentageCalculator()
    {
        if (!once)
        {
            once = true;
            yield return new WaitForSeconds(1f);
            List<GameObject> mainObjs = GameManager.instance.mainObjects;
            for (int i = 0; i < mainObjs.Count; i++)
            {
                //MultiplerAnimation[] temp;
                //temp = mainObjs[i].transform.childCount;
                float tempPercentage = (((float)(mainObjs[i].transform.childCount) / (float)gridObj.Count) * 100f);
                int perfectPercentage = Mathf.RoundToInt(tempPercentage);
                mainObjs[i].GetComponent<MainParent>().thisPlayerPercentage = perfectPercentage;
                Vector3 mainPlayerPos = mainObjs[i].GetComponent<MainParent>().mainPlayer.transform.position;
                GameObject mainBubblePop = Instantiate(bubblePop);
                mainBubblePop.transform.position = new Vector3(mainPlayerPos.x, mainPlayerPos.y + 1.5f, mainPlayerPos.z);
                mainBubblePop.transform.eulerAngles = new Vector3(50, 0, 0);
                mainBubblePop.GetComponent<FillBubbleTop>().percentage = perfectPercentage;
                mainObjs[i].GetComponent<MainParent>().fillBubble = mainBubblePop;
            }

            InvokeRepeating("WaveEffect",1,0.5f);
            // Checking for LevelComplete or fail check fillbubbletop script
            Camera.main.DOFieldOfView(70f, 0.85f); 

            //yield return new WaitForSeconds(2.5f);
            //GameManager.instance.CheckLevelCompleteORFail();
        }
    }


    int every = 0;
    void WaveEffect()
    {
        DOTween.KillAll();
        GameManager.instance.GettingAllCubes();
        List<GameObject> allcubes = GameManager.instance.allcubes;
        if (every == 0)
        {
            for (int i = 0; i < allcubes.Count; i++)
            {
                
                if (i % 3 == 0)
                    allcubes[i].transform.DOLocalMoveY(1.2f, 0.5f).SetEase(Ease.Flash);
                else
                    allcubes[i].transform.DOLocalMoveY(1.5f, 0.5f).SetEase(Ease.Flash);
            }
            every = 1;
        }
        else {
            for (int i = 0; i < allcubes.Count; i++)
            {
                if (i % 2 == 0)
                    allcubes[i].transform.DOLocalMoveY(1.2f, 0.5f).SetEase(Ease.Flash);
                else
                    allcubes[i].transform.DOLocalMoveY(1.5f, 0.5f).SetEase(Ease.Flash);
            }
            every = 0;
        }
        
    }
}
