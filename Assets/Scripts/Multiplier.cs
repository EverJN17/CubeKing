﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Multiplier : MonoBehaviour
{
    RaycastHit hit;
    public GameObject multiplierPrefab;
    GridCreation gc;

    void Start()
    {
        gc = FindObjectOfType<GridCreation>();
        Raycast_AllDirections();
    }


    void Raycast_AllDirections()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1))
        {
            if (hit.collider.tag.Equals("can"))
            {                    
                hit.collider.tag = "done";
                hit.collider.enabled = false;
                GameObject temp = Instantiate(Grid.instance.emptyObj, transform.position, Quaternion.identity);
                temp.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
                temp.AddComponent<MultiplerAnimation>();
                temp.GetComponent<MultiplerAnimation>().forward = true;
                temp.transform.parent = transform.parent;
                temp.tag = tag;
            }
        }
        if (Physics.Raycast(transform.position, -transform.forward, out hit, 1))
        {
            if (hit.collider.tag.Equals("can"))
            {
                hit.collider.tag = "done";
                hit.collider.enabled = false;
                GameObject temp = Instantiate(Grid.instance.emptyObj, transform.position, Quaternion.identity);
                temp.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
                temp.AddComponent<MultiplerAnimation>();
                temp.GetComponent<MultiplerAnimation>().backward = true;
                temp.transform.parent = transform.parent;
                temp.tag = tag;
            }
        }
        if (Physics.Raycast(transform.position, transform.right, out hit, 1))
        {
            if (hit.collider.tag.Equals("can"))
            {
                hit.collider.tag = "done";
                hit.collider.enabled = false;
                GameObject temp = Instantiate(Grid.instance.emptyObj, transform.position, Quaternion.identity);
                temp.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
                temp.AddComponent<MultiplerAnimation>();
                temp.GetComponent<MultiplerAnimation>().right = true;
                temp.transform.parent = transform.parent;
                temp.tag = tag;
            }
        }
        if (Physics.Raycast(transform.position, -transform.right, out hit, 1))
        {
            if (hit.collider.tag.Equals("can"))
            {
                hit.collider.tag = "done";
                hit.collider.enabled = false;
                GameObject temp = Instantiate(Grid.instance.emptyObj, transform.position, Quaternion.identity);
                temp.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
                temp.AddComponent<MultiplerAnimation>();
                temp.GetComponent<MultiplerAnimation>().left = true;
                temp.transform.parent = transform.parent;
                temp.tag = tag;
            }
        }
        enabled = false;
    }

    
   
}
