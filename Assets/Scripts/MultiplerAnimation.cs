﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TapticPlugin;

public class MultiplerAnimation : MonoBehaviour
{
    public bool left, right, forward, backward, isPlayer;
    RaycastHit hit;
    int speed;

    // Start is called before the first frame update
    void Start()
    {
        speed = 10;
        CallingRotation();
    }

    void CallingRotation()
    {
        if(AudioManager.instance)
            AudioManager.instance.PlayRotationSound();
        if (left)
        {
            Vector3 point = new Vector3(transform.position.x - 0.5f, 0.5f, transform.position.z);
            StartCoroutine(RotateAround(transform, point, transform.forward, speed));
        }
        else if (right)
        {
            Vector3 point = new Vector3(transform.position.x + 0.5f, 0.5f, transform.position.z);
            StartCoroutine(RotateAround(transform, point, -transform.forward, speed));
        }
        else if (forward)
        {
            Vector3 point = new Vector3(transform.position.x, 0.5f, transform.position.z + 0.5f);
            StartCoroutine(RotateAround(transform, point, transform.right, speed));
        }
        else if (backward)
        {
            Vector3 point = new Vector3(transform.position.x, 0.5f, transform.position.z - 0.5f);
            StartCoroutine(RotateAround(transform, point, -transform.right, speed));
        }
    }

    IEnumerator RotateAround(Transform objTransform, Vector3 point, Vector3 axis, int speed)
    {

        TapticManager.Impact(ImpactFeedback.Light);
       
        for (int i = 0; i < 90; i += speed)
        {
            objTransform.RotateAround(point, axis, speed);
            yield return null;
        }
        Resetting();

        CastingRayinDirection();
    }

    void Resetting()
    {
        left = false;
        backward = false;
        forward = false;
        right = false;
        transform.rotation = Quaternion.identity;
        transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), 
                                         Mathf.RoundToInt(transform.position.y),
                                         Mathf.RoundToInt(transform.position.z));
    }

    void CastingRayinDirection()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1))
        {
            if (hit.collider.tag.Equals("can"))
            {
                hit.collider.tag = "done";
                hit.collider.enabled = false;
                GameObject temp = Instantiate(Grid.instance.emptyObj, transform.position, Quaternion.identity);
                temp.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
                temp.AddComponent<MultiplerAnimation>();
                temp.GetComponent<MultiplerAnimation>().forward = true;
                temp.transform.parent = transform.parent;
                temp.tag = tag;    
            }
        }
        if (Physics.Raycast(transform.position, -transform.forward, out hit, 1))
        {
            if (hit.collider.tag.Equals("can"))
            {
                hit.collider.tag = "done";
                hit.collider.enabled = false;
                GameObject temp = Instantiate(Grid.instance.emptyObj, transform.position, Quaternion.identity);
                temp.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
                temp.AddComponent<MultiplerAnimation>();
                temp.GetComponent<MultiplerAnimation>().backward = true;
                temp.transform.parent = transform.parent;
                temp.tag = tag;
            }
        }
        if (Physics.Raycast(transform.position, transform.right, out hit, 1))
        {
            if (hit.collider.tag.Equals("can"))
            {
                hit.collider.tag = "done";
                hit.collider.enabled = false;
                GameObject temp = Instantiate(Grid.instance.emptyObj, transform.position, Quaternion.identity);
                temp.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
                temp.AddComponent<MultiplerAnimation>();
                temp.GetComponent<MultiplerAnimation>().right = true;
                temp.transform.parent = transform.parent;
                temp.tag = tag;
            }
        }
        if (Physics.Raycast(transform.position, -transform.right, out hit, 1))
        {
            if (hit.collider.tag.Equals("can"))
            {
                hit.collider.tag = "done";
                hit.collider.enabled = false;
                GameObject temp = Instantiate(Grid.instance.emptyObj, transform.position, Quaternion.identity);
                temp.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
                temp.AddComponent<MultiplerAnimation>();
                temp.GetComponent<MultiplerAnimation>().left = true;
                temp.transform.parent = transform.parent;
                temp.tag = tag;
            }
        }

        // Check for Level Percentage
        Grid.instance.CheckForLevelCompleteORFail();
        enabled = false;
    }

   

    
}
