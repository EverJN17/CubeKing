﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class StartScene : MonoBehaviour
{
    public static StartScene instance;
    public int level;
    // Start is called before the first frame update
    void Start()
    {
        level = PlayerPrefs.GetInt("level",1);
        if (level > 101)
            level = 1;
                
        SceneManager.LoadScene(level.ToString());
    }

}
