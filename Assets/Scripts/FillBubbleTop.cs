﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FillBubbleTop : MonoBehaviour
{
    public static FillBubbleTop instance;
    public SpriteRenderer crown;
    public TextMesh percentageTxt;
    public int percentage;
    public bool activateCrown, calCompleted;
    int tempPer = 0;
    
    private void Awake()
    {
        instance = this;
        percentageTxt = GetComponentInChildren<TextMesh>();
        InvokeRepeating("PercentageShower", 1, 0.01f);
    }
    

    void PercentageShower()
    {
        if (tempPer < percentage)
        {
            tempPer++;
            percentageTxt.text = tempPer + " %";
        }
        else {
            CancelInvoke();
            calCompleted = true;
            // level complete and fail
            if (GameManager.instance.CheckForPercentageCalculator())
                GameManager.instance.Invoke("CheckLevelCompleteORFail",0.5f);
        }
    }
}
