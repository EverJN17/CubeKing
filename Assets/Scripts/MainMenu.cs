﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] Image ImgMuted;
    [SerializeField] Image ImgUnMuted;

    private bool isMuted;

    void Start(){
        isMuted=false;
        UpdateMuteButton();
        AudioListener.pause = isMuted;
    }

    public void StartGame(){

    SceneManager.LoadScene("StartScene");

    }

    public void OnMuteButtonPress(){
        if(isMuted==false){
            isMuted = true;
            AudioListener.pause = true;
        }
        else {
            isMuted = false;     
            AudioListener.pause = false;
        }
        UpdateMuteButton();
    }
    private void UpdateMuteButton() {
        if (isMuted == false){
            ImgMuted.enabled = true;
            ImgUnMuted.enabled = false;
        }
        else {
            ImgMuted.enabled = false;
            ImgUnMuted.enabled = true;
        }

    }

    public void OnSharingButtonPressed(){
        Sharing.ShareURL("https://play.google.com/store/apps/details?id=com.tp.binvader");
        Debug.Log("Shared");
    }
    public void OnRatingButtonPressed(){
        StoreReview.RequestRating();
        Debug.Log("Button Rating Request");
    }
}
