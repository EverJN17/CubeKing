﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class LevelComplete : MonoBehaviour
{
    public static LevelComplete instance;
    public Image mytag, mysmiley;
    public Sprite[] tags, smileys;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.confetti.SetActive(true);
        mytag.sprite = tags[Random.Range(0, tags.Length)];
        mytag.SetNativeSize();
        mysmiley.sprite = smileys[Random.Range(0, smileys.Length)];
        int level = int.Parse(SceneManager.GetActiveScene().name);
        PlayerPrefs.SetInt("level", level + 1);

        mysmiley.GetComponent<RectTransform>().DOJumpAnchorPos(new Vector2(mysmiley.GetComponent<RectTransform>().anchoredPosition.x, -350f), 2f, 1, 0.5f).SetLoops(-1, LoopType.Yoyo);
        mysmiley.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, -18f), 1f).SetLoops(-1, LoopType.Yoyo);

        FindObjectOfType<AdManager>().ShowAdmobInterstitial();
    }

   

    public void AdsComplete()
    {
        PlayerPrefs.SetInt("LevelCompleteads", PlayerPrefs.GetInt("LevelCompleteads", 0) + 1);
        if (PlayerPrefs.GetInt("LevelCompleteads") > 5)
        {

            PlayerPrefs.SetInt("LevelCompleteads", 0);
        }
    }

}
