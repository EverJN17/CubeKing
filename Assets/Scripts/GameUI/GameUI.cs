﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class GameUI : MonoBehaviour
{
    public static GameUI instance;
    public GameObject menu, ingame, levelfail, levelcomplete;
    public TextMeshProUGUI levelNum;
    public Text movesTxt;
    GameManager gm;
   // public AudioSource audioDong;
   

    private void Awake()
    {
        instance = this;
        Application.targetFrameRate = 60;
    }

    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.instance;
        MovesCounter(gm.playerCreationCubes + 1);
        levelNum.text ="LEVEL - " + SceneManager.GetActiveScene().name;
        if (PlayerPrefs.GetInt("once", 0) == 1)
        {
            TapToPlay();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MovesCounter(int val)
    {
        movesTxt.text = (val).ToString();
    }

    public void Restart()
    {
        DOTween.KillAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Retry()
    {
        DOTween.KillAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void BackHome()
    {
        DOTween.KillAll();
        //Destroy(standbyCamera.GetComponent<AudioListener>());
        SceneManager.LoadScene("Menu");
    }

    public void Next()
    {
        Grid.instance.CancelInvoke();
        DOTween.KillAll();
        int level = int.Parse(SceneManager.GetActiveScene().name)+1;
        if (level.ToString() == "101")
            level = 1;
        Loading.levelToBeLoaded = level;
        SceneManager.LoadScene("LoadingScene");
    }

    public void TapToPlay()
    {
        menu.SetActive(false);
        ingame.SetActive(true);
        PlayerPrefs.SetInt("once", 1);
        GameManager.instance.gameStart = true;
    }

    public void LevelCompleted()
    {
        levelcomplete.SetActive(true);
        levelfail.SetActive(false);
        ingame.SetActive(false);
        AudioManager.instance.PlayLevelComplete();
    }
    public void LevelFailed()
    {
        levelcomplete.SetActive(false);
        levelfail.SetActive(true);
        ingame.SetActive(false);
        AudioManager.instance.PlayLevelFail();
    }

    public void PrivacyPolicy()
    {
        Application.OpenURL("https://unconditionalgames.s3.ap-south-1.amazonaws.com/UnConditionalPrivacyPolicy.txt");
    }

}
