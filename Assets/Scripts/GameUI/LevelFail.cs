﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class LevelFail : MonoBehaviour
{
    public static LevelFail instance;
    public Image mytag, mysmiley;
    public Sprite[] tags, smileys;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        mytag.sprite = tags[Random.Range(0, tags.Length)];
        mytag.SetNativeSize();
        mysmiley.sprite = smileys[Random.Range(0, smileys.Length)];
        mysmiley.transform.DOShakeScale(1, 0.35f, 3, 85).SetLoops(-1,LoopType.Yoyo);
        int level = int.Parse(SceneManager.GetActiveScene().name);

        FindObjectOfType<AdManager>().ShowAdmobInterstitial();
    }

    public void AdsFail()
    {
        PlayerPrefs.SetInt("LevelFailads", PlayerPrefs.GetInt("LevelFailads", 0) + 1);
        if (PlayerPrefs.GetInt("LevelFailads") > 3)
        {

            PlayerPrefs.SetInt("LevelFailads", 0);
        }
    }

}
