﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class Loading : MonoBehaviour
{
    public static int levelToBeLoaded;
    public Text loadingTxt;

    // Start is called before the first frame update
    void Start()
    {
        loadingTxt.DOFade(0, 0.75f).SetLoops(-1, LoopType.Yoyo);
        StartCoroutine("LoadingScene");
    }

    IEnumerator LoadingScene()
    {
        yield return new WaitForSeconds(1);
       AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(levelToBeLoaded.ToString());
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
