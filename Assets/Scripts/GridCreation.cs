﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCreation : MonoBehaviour
{
    public static GridCreation instance;
    public GameObject cube;
    public List<GameObject> grid = new List<GameObject>();
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                GameObject temp = Instantiate(cube, new Vector3(i, 0, j), Quaternion.identity);
                temp.name = "Grid " + i + "" + j;
                grid.Add(temp);
            }
        }
    }

}
